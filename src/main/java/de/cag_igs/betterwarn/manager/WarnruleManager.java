package de.cag_igs.betterwarn.manager;


import de.cag_igs.betterwarn.data.Warnrule;
import de.cag_igs.betterwarn.database.DatabaseFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WarnruleManager {
    public int getIdByName(String name) {
        ResultSet results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT `id` FROM `betterwarn_warnrules` " +
                "WHERE `name`=?", name);
        try {
            if (results.next()) {
                return results.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void addWarnrule(String name, String reason, int time, int penaltyPoints) {
        DatabaseFactory.getDatabaseObject().doSafeUpdate("INSERT INTO `betterwarn_warnrules` (`name`, `reason`, " +
                "`time`, `penaltyPoints`) VALUES(?, ?, ?, ?)", name, reason, Integer.toString(time), Integer.toString(penaltyPoints));
    }

    public void addWarnrule(Warnrule warnrule) {
        addWarnrule(warnrule.getName(), warnrule.getReason(), warnrule.getTime(), warnrule.getPenaltyPoints());
    }

    public void deleteWarnrule(int id) {
        DatabaseFactory.getDatabaseObject().doSafeUpdate("DELETE FROM `betterwarn_warnrules` WHERE `id`=?", Integer
                .toString(id));
    }

    public Warnrule getWarnrule(int id) {
        ResultSet results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT `name`, `reason`, `time`, `penaltyPoints` FROM " +
                "`betterwarn_warnrules` WHERE `id`=?", Integer.toString(id));
        Warnrule rule = null;
        try {
            if (results.next()) {
                rule = new Warnrule(id, results.getString("name"), results.getString("reason"), results.getInt("time"), results.getInt("penaltyPoints"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rule;
    }


    public List<Warnrule> getWarnrules() {
        ResultSet results = DatabaseFactory.getDatabaseObject().doQuery("SELECT * FROM `betterwarn_warnrules`");
        List<Warnrule> warnrules = new ArrayList<>();
        try {
            while (results.next()) {
                Warnrule temp = new Warnrule(results.getInt("id"), results.getString("name"),
                        results.getString("reason"), results.getInt("time"), results.getInt("penaltyPoints"));
                warnrules.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return warnrules;
    }
}
