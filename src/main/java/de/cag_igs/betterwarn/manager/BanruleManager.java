package de.cag_igs.betterwarn.manager;

import de.cag_igs.betterwarn.data.Banrule;
import de.cag_igs.betterwarn.database.DatabaseFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class BanruleManager {

    public int getIdByName(String name) {
        ResultSet results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT `id` FROM `betterwarn_banrules` " +
                "WHERE `name`=?", name);
        try {
            if (results.next()) {
                return results.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void addBanrule(String name, String reason, int time) {
        DatabaseFactory.getDatabaseObject().doSafeUpdate("INSERT INTO `betterwarn_banrules` (`name`, `reason`, " +
                "`time`) VALUES(?, ?, ?)", name, reason, Integer.toString(time));
    }

    public void addBanrule(Banrule banrule) {
        addBanrule(banrule.getName(), banrule.getReason(), banrule.getTime());
    }

    public void deleteBanrule(int id) {
        DatabaseFactory.getDatabaseObject().doSafeUpdate("DELETE FROM `betterwarn_banrules` WHERE `id`=?", Integer
                .toString(id));
    }

    public Banrule getBanrule(int id) {
        ResultSet results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT `name`, `reason`, `time` FROM " +
                "`betterwarn_banrules` WHERE `id`=?", Integer.toString(id));
        Banrule rule = null;
        try {
            if (results.next()) {
                rule = new Banrule(id, results.getString("name"), results.getString("reason"), results.getInt("time"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rule;
    }


    public List<Banrule> getBanrules() {
        ResultSet results = DatabaseFactory.getDatabaseObject().doQuery("SELECT * FROM `betterwarn_banrules`");
        List<Banrule> banrules = new ArrayList<>();
        try {
            while (results.next()) {
                Banrule temp = new Banrule(results.getInt("id"), results.getString("name"),
                        results.getString("reason"), results.getInt("time"));
                banrules.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return banrules;
    }
}
