package de.cag_igs.betterwarn.manager;


import de.cag_igs.betterwarn.Utility;
import de.cag_igs.betterwarn.data.PlayerWarn;
import de.cag_igs.betterwarn.data.Warnrule;
import de.cag_igs.betterwarn.database.DatabaseFactory;
import de.cag_igs.betterwarn.spigot.event.PlayerWarnEvent;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WarnManager {

    private String prefix;
    public WarnManager(String pref) {
        this.prefix = pref;
    }

    public List<PlayerWarn> getWarns(boolean active, UUID playeruuid, boolean local) {
        ResultSet results = null;
        if (!local) {
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `betterwarn_warns` WHERE " +
                    "`active`=? AND `playeruuid`=?", Integer.toString((active) ? 1 : 0), playeruuid.toString());
        } else {
            String localprefix = prefix;
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `" + localprefix + "warns` WHERE " +
                    "`active`=? AND `playeruuid`=?", Integer.toString((active) ? 1 : 0), playeruuid.toString());
        }
        if (results == null) {
            return null;
        } else {
            try {
                List<PlayerWarn> templist = new ArrayList<>();
                while (results.next()) {
                    PlayerWarn temp = new PlayerWarn(results.getInt("id"), results.getString("playerUuid"),
                            results.getString("executerUuid"), results.getString("reason"),
                            results.getInt("bantime"), results.getInt("time"),
                            results.getBoolean("active"), results.getInt("penaltyPoints"));
                    templist.add(temp);
                }
                return templist;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public List<PlayerWarn> getWarns(boolean active, boolean local) {
        ResultSet results = null;
        if (!local) {
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `betterwarn_warns` WHERE " +
                    "`active`=?", Integer.toString((active) ? 1 : 0));
        } else {
            String localprefix = prefix;
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `" + localprefix + "warns` WHERE " +
                    "`active`=?", Integer.toString((active) ? 1 : 0));
        }

        if (results == null) {
            return null;
        } else {
            try {
                List<PlayerWarn> templist = new ArrayList<>();
                while (results.next()) {
                    PlayerWarn temp = new PlayerWarn(results.getInt("id"), results.getString("playerUuid"),
                            results.getString("executerUuid"), results.getString("reason"),
                            results.getInt("bantime"), results.getInt("time"),
                            results.getBoolean("active"), results.getInt("penaltyPoints"));
                    templist.add(temp);
                }
                return templist;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public void validateWarn(int id, boolean local) {
        int currentTime = Utility.getCurrentUnixTimeStamp();
        PlayerWarn warn = getWarn(id, local);
        if (currentTime >= warn.getBantime() + warn.getTime() && warn.getTime() != 0) {
            toggleWarn(id, false, local);
        } else {
            toggleWarn(id, true, local);
        }
    }

    public void validateWarns(boolean local) {
        List<PlayerWarn> warnlist = getWarns(true, local);
        for (PlayerWarn warn : warnlist) {
            validateWarn(warn.getId(), local);
        }

    }

    public void validateWarns(UUID playeruuid, boolean local) {
        List<PlayerWarn> warnlist = getWarns(true, playeruuid, local);
        for (PlayerWarn warn : warnlist) {
            validateWarn(warn.getId(), local);
        }
    }

    public PlayerWarn getWarn(int id, boolean local) {
        ResultSet results = null;
        if (!local) {
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `betterwarn_warns` WHERE " +
                    "`id`=?", Integer.toString(id));
        } else {
            String localprefix = prefix;
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `" + localprefix + "warns` WHERE " +
                    "`id`=?", Integer.toString(id));
        }

        if (results == null) {
            return null;
        } else {
            try {
                if (results.next()) {
                    PlayerWarn warn = new PlayerWarn(results.getInt("id"), results.getString("playerUuid"),
                            results.getString("executerUuid"), results.getString("reason"),
                            results.getInt("bantime"), results.getInt("time"),
                            results.getBoolean("active"), results.getInt("penaltyPoints"));
                    return warn;
                } else {
                    return null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public void addWarn(PlayerWarn warn, boolean local) {
        if (!local) {
            DatabaseFactory.getDatabaseObject().doSafeUpdate(
                    "INSERT INTO `betterwarn_warns` (`playerUuid`, `executerUuid`, `reason`, `bantime`, `time`, `active`, `penaltyPoints`) VALUES (?, ?, ?, ?, ?, ?, ?)",
                    warn.getPlayerUuid(), warn.getExecuterUuid(), warn.getReason(),
                    Integer.toString(warn.getBantime()), Integer.toString(warn.getTime()),
                    Integer.toString(1), Integer.toString(warn.getPenaltyPoints()));
        } else {
            String localprefix = prefix;
            //add local option
            DatabaseFactory.getDatabaseObject().doSafeUpdate(
                    "INSERT INTO `" + localprefix + "warns` (`playerUuid`, `executerUuid`, `reason`, `bantime`, `time`, `active`, `penaltyPoints`) VALUES (?, ?, ?, ?, ?, ?, ?)",
                    warn.getPlayerUuid(), warn.getExecuterUuid(), warn.getReason(),
                    Integer.toString(warn.getBantime()), Integer.toString(warn.getTime()),
                    Integer.toString(1), Integer.toString(warn.getPenaltyPoints()));
        }
        warn.setActive(true);
        //TODO dispatch PlayerWarnEvent
        Bukkit.getServer().getPluginManager().callEvent(new PlayerWarnEvent(warn));
    }

    public void addWarn(UUID playerUuid, UUID executerUuid, String reason, int bantime, int time, int penaltyPoints, boolean local) {
        PlayerWarn temp = new PlayerWarn(0, playerUuid.toString(), executerUuid.toString(), reason, bantime, time, true, penaltyPoints);
        addWarn(temp, local);
    }

    public void addWarn(UUID playerUuid, UUID executerUuid, String reason, int time, int penaltyPoints, boolean local) {
        PlayerWarn temp = new PlayerWarn(0, playerUuid.toString(), executerUuid.toString(), reason,
                Utility.getCurrentUnixTimeStamp(), time,true, penaltyPoints);
        addWarn(temp, local);
    }

    public void addWarn(UUID playerUuid, UUID executerUuid, int bantime, Warnrule rule, boolean local) {
        PlayerWarn temp = new PlayerWarn(0, playerUuid.toString(), executerUuid.toString(), rule.getReason(), bantime, rule.getTime(), true, rule.getPenaltyPoints());
        addWarn(temp, local);
    }

    public void addWarn(UUID playerUuid, UUID executerUuid, Warnrule rule, boolean local) {
        PlayerWarn temp = new PlayerWarn(0, playerUuid.toString(), executerUuid.toString(), rule.getReason(),
                Utility.getCurrentUnixTimeStamp(), rule.getTime(), true, rule.getPenaltyPoints());
        addWarn(temp, local);
    }

    public boolean toggleWarn(int id, boolean active, boolean local) {
        if (!local) {
            return DatabaseFactory.getDatabaseObject().doSafeUpdate("UPDATE `betterwarn_warns` SET `active`=? WHERE " +
                    "`id`=?", Integer.toString((active) ? 1 : 0), Integer.toString(id));
        } else {
            String localprefix = prefix;
            return DatabaseFactory.getDatabaseObject().doSafeUpdate("UPDATE `" + localprefix + "warns` SET `active`=? WHERE " +
                    "`id`=?", Integer.toString((active) ? 1 : 0), Integer.toString(id));
        }
    }
}
