package de.cag_igs.betterwarn.manager;


import de.cag_igs.betterwarn.Utility;
import de.cag_igs.betterwarn.data.Banrule;
import de.cag_igs.betterwarn.data.PlayerBan;
import de.cag_igs.betterwarn.database.DatabaseFactory;
import de.cag_igs.betterwarn.spigot.event.PlayerBanEvent;
import org.bukkit.Bukkit;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BanManager {
    private String prefix;

    public BanManager(String prefix) {
        this.prefix = prefix;
    }

    public List<PlayerBan> getBans(boolean active, UUID playeruuid, boolean local) {
        ResultSet results = null;
        if (local) {
            String localprefix = prefix;
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `" + localprefix + "bans` WHERE " +
                    "`active`=? AND `playeruuid`=?", Integer.toString((active) ? 1 : 0), playeruuid.toString());
        } else {
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `betterwarn_bans` WHERE " +
                    "`active`=? AND `playeruuid`=?", Integer.toString((active) ? 1 : 0), playeruuid.toString());
        }
        if (results == null) {
            return null;
        } else {
            try {
                List<PlayerBan> templist = new ArrayList<>();
                while (results.next()) {
                    PlayerBan temp = new PlayerBan(results.getInt("id"), results.getString("playerUuid"),
                            results.getString("executerUuid"), results.getString("reason"),
                            results.getInt("bantime"), results.getInt("time"),
                            results.getBoolean("active"));
                    templist.add(temp);
                }
                return templist;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public List<PlayerBan> getBans(boolean active, boolean local) {
        ResultSet results;
        if (local) {
            String localprefix = prefix;
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `" + localprefix + "bans` WHERE " +
                    "`active`=?", Integer.toString((active) ? 1 : 0));
        } else {
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `betterwarn_bans` WHERE " +
                    "`active`=?", Integer.toString((active) ? 1 : 0));
        }
        if (results == null) {
            return null;
        } else {
            try {
                List<PlayerBan> templist = new ArrayList<>();
                while (results.next()) {
                    PlayerBan temp = new PlayerBan(results.getInt("id"), results.getString("playerUuid"),
                            results.getString("executerUuid"), results.getString("reason"),
                            results.getInt("bantime"), results.getInt("time"),
                            results.getBoolean("active"));
                    templist.add(temp);
                }
                return templist;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public void validateBan(int id, boolean local) {
        Bukkit.getLogger().info("Validating ban!");
        int currentTime = Utility.getCurrentUnixTimeStamp();
        PlayerBan ban = getBan(id, local);
        if ((currentTime >= (ban.getBantime() + ban.getTime())) && ban.getTime() != 0) {
            Bukkit.getLogger().info("Ban expired! Performing update!");
            toggleBan(id, false, local);
        } else {
            toggleBan(id, true, local);
        }
    }

    public void validateBans(boolean local) {
        Bukkit.getLogger().info("Validate all bans!");
        List<PlayerBan> banlist = getBans(true, local);
        for (PlayerBan ban : banlist) {
            validateBan(ban.getId(), local);
        }
    }

    public void validateBans(UUID playeruuid, boolean local) {
        List<PlayerBan> banlist = getBans(true, playeruuid, local);
        for (PlayerBan ban : banlist) {
            validateBan(ban.getId(), local);
        }
    }

    public PlayerBan getBan(int id, boolean local) {
        ResultSet results;
        if (local) {
            String localprefix = prefix;
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `" + localprefix + "bans` WHERE `id`=?", Integer.toString(id));
        } else {
            results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT * FROM `betterwarn_bans` WHERE " +
                    "`id`=?", Integer.toString(id));
        }
        if (results == null) {
            return null;
        } else {
            try {
                if (results.next()) {
                    return new PlayerBan(results.getInt("id"), results.getString("playerUuid"),
                            results.getString("executerUuid"), results.getString("reason"),
                            results.getInt("bantime"), results.getInt("time"),
                            results.getBoolean("active"));
                } else {
                    return null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public void addBan(PlayerBan ban, boolean local) {
        if (local) {
            String localprefix = prefix;
            DatabaseFactory.getDatabaseObject().doSafeUpdate(
                    "INSERT INTO `" + localprefix + "bans` (`playerUuid`, `executerUuid`, `reason`, `bantime`, `time`, `active`)" +
                            " VALUES (?, ?, ?, ?, ?, ?)", ban.getPlayerUuid(), ban.getExecuterUuid(), ban.getReason(),
                    Integer.toString(ban.getBantime()), Integer.toString(ban.getTime()), Integer.toString(1));
        } else {
            DatabaseFactory.getDatabaseObject().doSafeUpdate(
                    "INSERT INTO `betterwarn_bans` (`playerUuid`, `executerUuid`, `reason`, `bantime`, `time`, `active`)" +
                            " VALUES (?, ?, ?, ?, ?, ?)", ban.getPlayerUuid(), ban.getExecuterUuid(), ban.getReason(),
                    Integer.toString(ban.getBantime()), Integer.toString(ban.getTime()), Integer.toString(1));
        }


        ban.setActive(true);
        Bukkit.getServer().getPluginManager().callEvent(new PlayerBanEvent(ban));
    }

    public void addBan(UUID playerUuid, UUID executerUuid, String reason, int bantime, int time, boolean local) {
        addBan(new PlayerBan(0, playerUuid.toString(), executerUuid.toString(), reason, bantime, time, true), local);
    }

    public void addBan(UUID playerUuid, UUID executerUuid, String reason, int time, boolean local) {
        addBan(new PlayerBan(0, playerUuid.toString(), executerUuid.toString(), reason, Utility.getCurrentUnixTimeStamp(), time, true), local);
    }

    public void addBan(UUID playerUuid, UUID executerUuid, int bantime, Banrule banrule, boolean local) {
        addBan(new PlayerBan(0, playerUuid.toString(), executerUuid.toString(), banrule.getReason(), bantime,
                banrule.getTime(), true), local);
    }

    public void addBan(UUID playerUuid, UUID executerUuid, Banrule banrule, boolean local) {
        addBan(new PlayerBan(0, playerUuid.toString(), executerUuid.toString(), banrule.getReason(), Utility.getCurrentUnixTimeStamp(),
                banrule.getTime(), true), local);
    }

    public boolean toggleBan(int id, boolean active, boolean local) {
        if (local) {
            String localprefix = prefix;
            return DatabaseFactory.getDatabaseObject().doSafeUpdate("UPDATE `" + localprefix + "bans` SET `active`=? WHERE " +
                    "`id`=?", Integer.toString((active) ? 1 : 0), Integer.toString(id));
        } else {
            return DatabaseFactory.getDatabaseObject().doSafeUpdate("UPDATE `betterwarn_bans` SET `active`=? WHERE " +
                    "`id`=?", Integer.toString((active) ? 1 : 0), Integer.toString(id));
        }


    }


}
