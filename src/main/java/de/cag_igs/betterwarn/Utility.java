package de.cag_igs.betterwarn;


import de.cag_igs.betterwarn.database.DatabaseFactory;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Utility {
    public static int getCurrentUnixTimeStamp() {
        return (int) (System.currentTimeMillis() / 1000L);
    }

    public static String getUsernameByUuid(String uuid) {
        ResultSet resultSet = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT `name` FROM `betterwarn_names`" +
                " WHERE `uuid`=?", uuid);
        try {
            if (resultSet.next()) {
                return resultSet.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUuidByName(String name) {
        ResultSet resultSet = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT `uuid` FROM `betterwarn_names`" +
                " WHERE `name`=?", name);
        try {
            if (resultSet.next()) {
                return resultSet.getString("uuid");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
