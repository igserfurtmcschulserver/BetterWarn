package de.cag_igs.betterwarn.spigot.listener;

import de.cag_igs.betterwarn.data.PlayerBan;
import de.cag_igs.betterwarn.database.DatabaseFactory;
import de.cag_igs.betterwarn.manager.BanManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import java.util.List;
import java.util.logging.Logger;

public class PlayerPreLoginListener implements Listener {

    private BanManager banManager;
    private Logger logger;

    public PlayerPreLoginListener(BanManager bm, Logger logger) {
        banManager = bm;
        this.logger = logger;
    }

    @EventHandler
    public void onPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        logger.info("PlayerPreLogin!");
        banManager.validateBans(event.getUniqueId(), false);
        List<PlayerBan> banList = banManager.getBans(true, event.getUniqueId(), false);
        logger.info("Checking for (global) bans!");
        if (banList == null || banList.isEmpty()) {
            //Sollte der Spiele nicht global gebannt sein, geht es darum lokale Bans zu überprüfen
            banManager.validateBans(event.getUniqueId(), true);
            banList = banManager.getBans(true, event.getUniqueId(), true);
            logger.info("Checking for lokal Bans!");
        }
        if (banList != null && !banList.isEmpty()) {
            //Wird eine Liste mit Spielerbans zur�ckgegeben (ist nicht null) ist er definitv gebannt.
            //F�r die Bannnachricht ist lediglich der erste Eintrag der Bannliste notwendig; Alle anderen Banns haben in dem Sinne keine Auswirkung
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, banList.get(0).getReason());
            logger.info(event.getName() + " was banned!");
        }

        DatabaseFactory.getDatabaseObject().doSafeUpdate("INSERT INTO `betterwarn_names` (`uuid`, `name`) VALUES" +
                " (?, ?) ON DUPLICATE KEY UPDATE `name`=?", event.getUniqueId().toString(), event
                        .getName
                        (),
                event.getName());
    }

}
