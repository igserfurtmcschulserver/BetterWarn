package de.cag_igs.betterwarn.spigot.listener;

import de.cag_igs.betterwarn.spigot.SpigotMain;
import de.cag_igs.betterwarn.spigot.event.PlayerBanEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;

/**
 * Created by Markus on 02.06.2017.
 */
public class PlayerBanListener implements Listener{
    private SpigotMain main;

    public PlayerBanListener(SpigotMain main) {
        this.main = main;
    }

    @EventHandler
    public void onPlayerBanEvent(PlayerBanEvent event) {
        if (event.getBan().isActive()) {
            for (Player player : main.getServer().getOnlinePlayers()) {
                if (event.getBan().getPlayerUuid().equals(player.getUniqueId().toString())) {
                    player.kickPlayer(event.getBan().getReason());
                }
            }
        }
    }
}
