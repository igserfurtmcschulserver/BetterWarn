package de.cag_igs.betterwarn.spigot.listener;

import de.cag_igs.betterwarn.Utility;
import de.cag_igs.betterwarn.spigot.event.PlayerWarnEvent;
import de.cag_igs.betterwarn.data.PlayerWarn;
import de.cag_igs.betterwarn.manager.BanManager;
import de.cag_igs.betterwarn.manager.WarnManager;
import de.cag_igs.betterwarn.spigot.SpigotMain;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;
import java.util.UUID;


public class PlayerWarnListener implements Listener {

    private WarnManager warnManager;
    private BanManager banManager;
    private SpigotMain main;

    public PlayerWarnListener(WarnManager wm, BanManager bm, SpigotMain main) {
        warnManager = wm;
        banManager = bm;
        this.main = main;
    }

    @EventHandler
    public void onPlayerWarnEvent(PlayerWarnEvent event) {
        main.getServer().dispatchCommand(main.getServer().getConsoleSender(), "/mail send " + Utility
                .getUsernameByUuid(event.getWarn().getPlayerUuid()) + " Du wurdest von " + Utility
                .getUsernameByUuid(event.getWarn().getExecuterUuid()) + " verwarnt! Grund: " + event.getWarn()
                .getReason());
        main.getServer().dispatchCommand(main.getServer().getConsoleSender(), "/mail send " + Utility
                .getUsernameByUuid(event.getWarn().getPlayerUuid()) + " Deine Verwarnungen kannst du dir mit \"/warn" +
                " list\" anzeigen lassen");
        warnManager.validateWarns(UUID.fromString(event.getWarn().getPlayerUuid()), false);
        warnManager.validateWarns(UUID.fromString(event.getWarn().getPlayerUuid()), true);
        List<PlayerWarn> warnlist = warnManager.getWarns(true, UUID.fromString(event.getWarn().getPlayerUuid()),
                false);
        int penaltyPoints = 0;
        for (PlayerWarn warn : warnlist) {
            penaltyPoints += warn.getPenaltyPoints();
        }
        if (penaltyPoints >= main.getConfig().getInt("autoban.needwarncount") ) {
            banManager.addBan(UUID.fromString(event.getWarn().getPlayerUuid()), UUID.randomUUID(),
                    "Du wurdes wegen einer zu großen Anzahl an Verwarnungen global gebannt.", main.getConfig()
                            .getInt("autoban.bantime"), false);
        } else {
            warnlist = warnManager.getWarns(true, UUID.fromString(event.getWarn().getPlayerUuid()), true);
            for (PlayerWarn warn : warnlist) {
                penaltyPoints += warn.getPenaltyPoints();
            }
            if (penaltyPoints >= main.getConfig().getInt("autoban.needwarncount")) {
                banManager.addBan(UUID.fromString(event.getWarn().getPlayerUuid()), UUID.randomUUID(),
                        "Du wurdes wegen einer zu großen Anzahl an Verwarnungen lokal gebannt.", main.getConfig()
                                .getInt("autoban.bantime"), true);
            }
        }
    }
}
