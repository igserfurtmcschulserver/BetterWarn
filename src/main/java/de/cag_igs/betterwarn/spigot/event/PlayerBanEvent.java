package de.cag_igs.betterwarn.spigot.event;

import de.cag_igs.betterwarn.data.PlayerBan;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class PlayerBanEvent extends Event {

    private static final HandlerList HANDLERS = new HandlerList();
    private PlayerBan ban;

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    public PlayerBanEvent(PlayerBan ban) {
        this.ban = ban;
    }

    public PlayerBanEvent(boolean isAsync, PlayerBan ban) {
        super(isAsync);
        this.ban = ban;
    }

    /**
     * Returns a ban object which contains all important ban information.
     *
     * @return a PlayerBan object
     */
    public PlayerBan getBan() {
        return ban;
    }
}
