package de.cag_igs.betterwarn.spigot.event;

import de.cag_igs.betterwarn.data.PlayerWarn;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerWarnEvent extends Event{

    private static final HandlerList HANDLERS = new HandlerList();

    private PlayerWarn warn;

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    public PlayerWarnEvent(PlayerWarn warn) {
        this.warn = warn;
    }

    public PlayerWarnEvent(boolean isAsync, PlayerWarn warn) {
        super(isAsync);
        this.warn = warn;
    }

    /**
     * Returns a warn object which contains all important warn information.
     *
     * @return a PlayerWarn object
     */
    public PlayerWarn getWarn() {
        return warn;
    }
}
