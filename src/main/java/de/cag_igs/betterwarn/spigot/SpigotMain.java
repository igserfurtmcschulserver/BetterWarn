package de.cag_igs.betterwarn.spigot;

import de.cag_igs.betterwarn.data.Banrule;
import de.cag_igs.betterwarn.data.Warnrule;
import de.cag_igs.betterwarn.database.DatabaseFactory;
import de.cag_igs.betterwarn.manager.BanManager;
import de.cag_igs.betterwarn.manager.BanruleManager;
import de.cag_igs.betterwarn.manager.WarnManager;
import de.cag_igs.betterwarn.manager.WarnruleManager;
import de.cag_igs.betterwarn.spigot.command.BanCommand;
import de.cag_igs.betterwarn.spigot.command.BanruleCommand;
import de.cag_igs.betterwarn.spigot.command.WarnCommand;
import de.cag_igs.betterwarn.spigot.command.WarnruleCommand;
import de.cag_igs.betterwarn.spigot.listener.PlayerBanListener;
import de.cag_igs.betterwarn.spigot.listener.PlayerPreLoginListener;
import de.cag_igs.betterwarn.spigot.listener.PlayerWarnListener;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.UUID;


public class SpigotMain extends JavaPlugin {

    private WarnManager warnManager;
    private BanManager banManager;
    private WarnruleManager warnruleManager;
    private BanruleManager banruleManager;

    @Override
    public void onEnable() {
        loadConfig();
        DatabaseFactory.createDatabaseObject(this);

        warnManager = new WarnManager(getConfig().getString("local.database.prefix"));
        banManager = new BanManager(getConfig().getString("local.database.prefix"));
        warnruleManager = new WarnruleManager();
        banruleManager = new BanruleManager();


        createDatabaseTables();

        warnManager.validateWarns(false);
        warnManager.validateWarns(true);
        banManager.validateBans(false);
        banManager.validateBans(true);

        this.getServer().getPluginManager().registerEvents(new PlayerWarnListener(warnManager, banManager, this),
                this);
        this.getServer().getPluginManager().registerEvents(new PlayerPreLoginListener(banManager, getLogger()),
                this);
        this.getServer().getPluginManager().registerEvents(new PlayerBanListener(this), this);
        //this.getServer().getPluginManager().registerEvents(new PlayerPreLoginListener(banManager,
        //        runtimeInformation), this);
        //registerRulePermissionNodes();
        this.getCommand("warnrule").setExecutor(new WarnruleCommand());
        this.getCommand("banrule").setExecutor(new BanruleCommand());
        this.getCommand("ban").setExecutor(new BanCommand(banManager, getConfig().getString("serverfakeuuid")));
        this.getCommand("warn").setExecutor(new WarnCommand(warnManager, getConfig().getString("serverfakeuuid")));
        DatabaseFactory.getDatabaseObject().doSafeUpdate("INSERT INTO `betterwarn_names` (`uuid`, `name`) VALUES" +
                        " (?, ?) ON DUPLICATE KEY UPDATE `name`=?", getConfig().getString("serverfakeuuid"), "SERVER", "SERVER");
    }

    public void createDatabaseTables() {
        DatabaseFactory.getDatabaseObject().doUpdate("CREATE TABLE IF NOT EXISTS `betterwarn_names` (uuid VARCHAR" +
                "(36) NOT NULL PRIMARY KEY, name VARCHAR(40))");
        DatabaseFactory.getDatabaseObject().doUpdate("CREATE TABLE IF NOT EXISTS `betterwarn_bans` (id INT NOT NULL " +
                "PRIMARY KEY AUTO_INCREMENT, playerUuid VARCHAR(36), executerUuid VARCHAR(36), reason VARCHAR(300), " +
                "bantime INT, time INT, active BOOLEAN)");
        DatabaseFactory.getDatabaseObject().doUpdate("CREATE TABLE IF NOT EXISTS `betterwarn_warns` (id INT NOT NULL" +
                " PRIMARY KEY AUTO_INCREMENT, playerUuid VARCHAR(36), executerUuid VARCHAR(36), reason VARCHAR(300), " +
                "bantime INT, time INT, active BOOLEAN, penaltyPoints INT)");
        DatabaseFactory.getDatabaseObject().doUpdate("CREATE TABLE IF NOT EXISTS `" + getConfig().getString("local" +
                ".database.prefix") + "bans` (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, playerUuid VARCHAR(36), " +
                "executerUuid VARCHAR(36), reason VARCHAR(300), bantime INT, time INT, active BOOLEAN)");
        DatabaseFactory.getDatabaseObject().doUpdate("CREATE TABLE IF NOT EXISTS `" + getConfig().getString("local.database.prefix") + "warns` (id INT NOT NULL" +
                " PRIMARY KEY AUTO_INCREMENT, playerUuid VARCHAR(36), executerUuid VARCHAR(36), reason VARCHAR(300), " +
                "bantime INT, time INT, active BOOLEAN, penaltyPoints INT)");
        DatabaseFactory.getDatabaseObject().doUpdate("CREATE TABLE IF NOT EXISTS `betterwarn_banrules` (id INT NOT " +
                "NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(20), reason VARCHAR(300), time INT)");
        DatabaseFactory.getDatabaseObject().doUpdate("CREATE TABLE IF NOT EXISTS `betterwarn_warnrules` (id INT NOT " +
                "NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(20), reason VARCHAR(300), time INT, penaltyPoints INT)");
    }

    public void loadConfig() {
        getConfig().options().header("Config for betterwarn - spigot version");
        getConfig().addDefault("local.database.prefix", "localprefix");
        getConfig().addDefault("datastore.type", "mysql");
        getConfig().addDefault("autoban.needwarncount", 10);
        getConfig().addDefault("autoban.bantime", 604800);
        getConfig().addDefault("serverfakeuuid", UUID.randomUUID().toString());
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public void registerRulePermissionNodes() {
        List<Warnrule> warnrules = warnruleManager.getWarnrules();
        for (Warnrule rule : warnrules) {
            getDescription().getPermissions().add(new Permission("betterwarn.warnrule.rule." + rule.getId()));
        }

        List<Banrule> banrules = banruleManager.getBanrules();
        for (Banrule rule : banrules) {
            //getDescription().getPermissions().add(new Permission("betterwarn.banrule.rule." + rule.getId()));
        }

    }

    @Override
    public void onDisable() {
        DatabaseFactory.getDatabaseObject().close();
    }
}
