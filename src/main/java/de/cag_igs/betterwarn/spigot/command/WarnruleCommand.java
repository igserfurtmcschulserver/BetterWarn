package de.cag_igs.betterwarn.spigot.command;

import de.cag_igs.betterwarn.data.Warnrule;
import de.cag_igs.betterwarn.manager.WarnruleManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.List;


public class WarnruleCommand implements CommandExecutor {

    private WarnruleManager warnruleManager;

    public WarnruleCommand() {
        warnruleManager = new WarnruleManager();
    }

    private void paginateOutput(List<Warnrule> warnrules, int page, CommandSender executor) {
        int pages = warnrules.size() / 5;
        executor.sendMessage("ID) Regelname: Grund [Zeit] [Strafpunkte]");
        for (int i = page * 5; i < page * 5 + 5; ++i) {
            if (i < warnrules.size()) {
                executor.sendMessage("" + warnrules.get(i).getId() + ") " + warnrules.get(i).getName() + ": " +
                        warnrules.get(i).getReason() + " [" + warnrules.get(i).getTime() + "] [" + warnrules.get(i)
                        .getPenaltyPoints() + "]");
            }
        }
        executor.sendMessage("Seite: " + (page + 1) + "/" + (pages + 1));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!label.equalsIgnoreCase("warnrule") || args.length < 1) {
            return false;
        }

        if (args[0].equalsIgnoreCase("list")) {
            if (sender.hasPermission("betterwarn.warnrule.list")) {
                List<Warnrule> warnrules = warnruleManager.getWarnrules();
                int page = 0;
                if (args.length == 2) {
                    if (args[1].matches("\\d*")) {
                        page = Integer.parseInt(args[1]) - 1;
                        if (page < 0) {
                            page = 0;
                        }
                    }
                }
                if (warnrules != null) {
                    paginateOutput(warnrules, page, sender);
                } else {
                    sender.sendMessage("There are no Warnrules!");
                }
            }
            return true;
        } else if (args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("add")) {
            if (sender.hasPermission("betterwarn.warnrule.create")) {
                if (args.length <= 5) {
                    sender.sendMessage("/" + label + " create " + " <name> <time (in minutes)> <penalty points> " +
                            "<reason>");
                    return true;
                }
                String name = args[1];
                int time;
                try {
                    time = Integer.valueOf(args[2]) * 60;
                } catch (NumberFormatException exception) {
                    sender.sendMessage("/warnrule create <name> <time> <penalty points> <reason>");
                    return true;
                }
                int penaltyPoints;
                try {
                    penaltyPoints = Integer.valueOf(args[3]);
                } catch (NumberFormatException exception) {
                    sender.sendMessage("/warnrule create <name> <time> <penalty points> <reason>");
                    return true;
                }
                String message = "";
                for (int i = 4; i < args.length; ++i) {
                    message += args[i];
                    message += " ";
                }
                warnruleManager.addWarnrule(name, message, time, penaltyPoints);
            }
            return true;

        } else if (args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("delete")) {
            if (sender.hasPermission("betterwarn.warnrule.remove")) {
                if (args.length != 2) {
                    sender.sendMessage("Du musst eine ID angeben!");
                    return false;
                }
                int id;
                try {
                    id = Integer.valueOf(args[1]);
                } catch (NumberFormatException exception) {
                    sender.sendMessage("/warnrule remove <id>");
                    return true;
                }
                if (id >= 0) {
                    warnruleManager.deleteWarnrule(id);
                } else {
                    sender.sendMessage("Ungültige Warnregel ID!");
                    return true;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
