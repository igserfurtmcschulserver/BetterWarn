package de.cag_igs.betterwarn.spigot.command;

import de.cag_igs.betterwarn.Utility;
import de.cag_igs.betterwarn.data.PlayerBan;
import de.cag_igs.betterwarn.manager.BanManager;
import de.cag_igs.betterwarn.manager.BanruleManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;


public class BanCommand implements CommandExecutor {

    private BanManager banManager;
    private BanruleManager banruleManager;
    private String fakeuuid;

    public BanCommand(BanManager banManager, String uuid) {
        this.banManager = banManager;
        banruleManager = new BanruleManager();
        fakeuuid = uuid;
    }


    private void paginateOutput(List<PlayerBan> bans, int page, CommandSender executor) {
        int pages = bans.size() / 5;
        executor.sendMessage("ID) Spieler Grund Zeitpunkt Zeit Aktiv");
        for (int i = page * 5; i < page * 5 + 5; ++i) {
            if (bans.size() > i) {
                executor.sendMessage("" + bans.get(i).getId() + ") Spieler:" + Utility.getUsernameByUuid(bans.get(i).getPlayerUuid()
                ) + " Grund:" + bans.get(i).getReason() + " Zeitpunkt:" + bans.get(i).getBantime() + " Zeit:" + bans.get(i).getTime() + " Aktiv:" + bans.get(i).isActive());

            }
        }
        executor.sendMessage("Seite: " + (page + 1) + "/" + (pages + 1));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!label.equalsIgnoreCase("ban") || args.length < 1) {
            return false;
        }

        if (args[0].equalsIgnoreCase("list")) {
            List<PlayerBan> bans = null;
            int page = 0;
            if (sender.hasPermission("betterwarn.ban.list")) {
                if (args.length == 1) {
                    bans = banManager.getBans(false, false);
                    bans.addAll(banManager.getBans(true, false));
                } else if (args.length >= 2) {
                    if (args[1].matches("\\d*")) {
                        bans = banManager.getBans(false, false);
                        bans.addAll(banManager.getBans(true, false));
                        page = Integer.parseInt(args[1]) - 1;
                        if (page < 0) {
                            page = 0;
                        }
                    } else {
                        bans = banManager.getBans(false, UUID.fromString(Utility.getUuidByName(args[1])), false);
                        bans.addAll(banManager.getBans(true, UUID.fromString(Utility.getUuidByName(args[1])), false));
                        if (args.length == 3) {
                            if (args[1].matches("\\d*")) {
                                page = Integer.parseInt(args[1]) - 1;
                                if (page < 0) {
                                    page = 0;
                                }
                            }
                        }
                    }
                }
            }
            if (bans != null) {
                paginateOutput(bans, page, sender);
            } else {
                sender.sendMessage("Keine Bans vorhanden!");
            }
        } else if (args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("remove") || args[0]
                .equalsIgnoreCase("pardon")) {
            if (args.length != 2) {
                sender.sendMessage("Du musst eine ID angeben!");
                return false;
            }
            if (sender.hasPermission("betterwarn.ban.remove")) {
                try {
                    banManager.toggleBan(Integer.valueOf(args[1]), false, false);
                } catch (NumberFormatException exception) {
                    sender.sendMessage("/ban pardon <id>");
                    return true;
                }
                sender.sendMessage("Der Bann mit der ID " + args[1] + " wurde gelöscht!");
                return true;
            }
        } else {
            if (sender.hasPermission("betterwarn.ban.add")) {
                String uuid = Utility.getUuidByName(args[0]);
                String senderuuid = "";
                if (sender instanceof Player) {
                    senderuuid = ((Player) sender).getUniqueId().toString();
                } else {
                    senderuuid = fakeuuid;
                }
                if (args.length == 2) {
                    int ruleid = 0;
                    try {
                        ruleid = Integer.valueOf(args[1]);
                    } catch (NumberFormatException ex) {
                        ruleid = banruleManager.getIdByName(args[1]);
                    }
                    if (ruleid == -1 || ruleid == 0) {
                        sender.sendMessage("Ungültige Banregel!");
                        return true;
                    }
                    banManager.addBan(UUID.fromString(uuid), UUID.fromString(senderuuid), banruleManager.getBanrule
                        (ruleid), false);
                    return true;
                } else if (args.length >= 3){
                    int time;
                    try {
                        time = Integer.valueOf(args[1]);
                    } catch (NumberFormatException exception) {
                        sender.sendMessage("/ban <player> <time> <reason>");
                        return true;
                    }
                    String banmessage = "";
                    for (int i = 2; i < args.length; ++i) {
                        banmessage += args[i];
                        banmessage += " ";
                    }
                    banManager.addBan(UUID.fromString(uuid), UUID.fromString(senderuuid), banmessage, time * 60, false);
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}
