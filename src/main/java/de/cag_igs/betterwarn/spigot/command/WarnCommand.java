package de.cag_igs.betterwarn.spigot.command;

import de.cag_igs.betterwarn.Utility;
import de.cag_igs.betterwarn.data.PlayerWarn;
import de.cag_igs.betterwarn.manager.WarnManager;
import de.cag_igs.betterwarn.manager.WarnruleManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;


public class WarnCommand implements CommandExecutor{

    private WarnManager warnManager;
    private WarnruleManager warnruleManager;
    private String fakeuuid;

    public WarnCommand(WarnManager warnManager, String fakeuuid) {
        this.warnManager = warnManager;
        this.warnruleManager = new WarnruleManager();
        this.fakeuuid = fakeuuid;
    }


    private void paginateOutput(List<PlayerWarn> bans, int page, CommandSender executor, boolean mode) {
        int pages = bans.size() / 5;
        if (mode) {
            executor.sendMessage("ID) Spieler Grund Zeitpunkt Zeit Aktiv Strafpunkte");
        } else {
            executor.sendMessage("ID) Von Grund Zeitpunkt Zeit Aktiv Strafpunkte");
        }
        for (int i = page * 5; i < page * 5 + 5; ++i) {
            if (bans.size() > i) {
                if (mode) {
                    executor.sendMessage("" + bans.get(i).getId() + ") Spieler:" + Utility.getUsernameByUuid(bans.get(i).getPlayerUuid()
                    ) + " Grund:" + bans.get(i).getReason() + " Zeitpunkt:" + bans.get(i).getBantime() + " Zeit:" + bans.get(i).getTime() + " Aktiv:" + bans.get(i).isActive() + " Strafpunkte: " + bans.get(i).getPenaltyPoints());
                } else {
                    executor.sendMessage("" + bans.get(i).getId() + ") Von:" + Utility.getUsernameByUuid(bans.get(i).getExecuterUuid()
                    ) + " Grund:" + bans.get(i).getReason() + " Zeitpunkt:" + bans.get(i).getBantime() + " Zeit:" + bans.get(i).getTime() + " Aktiv:" + bans.get(i).isActive() + " Strafpunkte: " + bans.get(i).getPenaltyPoints());
                }
            }
        }
        executor.sendMessage("Seite: " + (page + 1) + "/" + (pages + 1));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!label.equalsIgnoreCase("warn") || args.length < 1) {
            return false;
        }

        if (args[0].equalsIgnoreCase("list")) {
            int page = 0;
            List<PlayerWarn> warns = null;

            if (args.length >= 2 && !args[1].matches("\\d*")) {
                if (sender.hasPermission("betterwarn.warn.list")) {
                    if (args.length == 3 && args[2].matches("\\d*")) {
                        page = Integer.valueOf(args[2]);
                    }
                    warns = warnManager.getWarns(true, UUID.fromString(Utility.getUuidByName(args[1])), false);
                    warns.addAll(warnManager.getWarns(true, UUID.fromString(Utility.getUuidByName(args[1])), true));
                    if (warns != null) {
                        paginateOutput(warns, page, sender, false);
                    }
                } else {
                    return true;
                }
            } else {
                if (args.length >= 2) {
                    page = Integer.valueOf(args[1]);
                }
                if (sender instanceof Player) {
                    if (sender.hasPermission("betterwarn.warn.list.own")) {
                        Player player = (Player) sender;
                        warns = warnManager.getWarns(true, player.getUniqueId(), false);
                        warns.addAll(warnManager.getWarns(true, player.getUniqueId(), true));
                        if (warns != null) {
                            paginateOutput(warns, page, sender, false);
                            return true;
                        }
                    } else {
                        return true;
                    }
                } else {
                    sender.sendMessage("Götter können nicht verwarnt werden!");
                    return true;
                }
            }
        } else if (args[0].equalsIgnoreCase("pardon")) {
            if (args.length == 2 && args[1].matches("\\d*") && sender.hasPermission("betterwarn.warn.pardon")) {
                int id = Integer.valueOf(args[1]);
                warnManager.toggleWarn(id, false, false);
                return true;
            } else {
                sender.sendMessage("/warn pardon <id>");
                return true;
            }
        } else {
            if (sender.hasPermission("betterwarn.warn.add")) {
                String uuid = Utility.getUuidByName(args[0]);
                String senderuuid = "";
                if (sender instanceof Player) {
                    senderuuid = ((Player) sender).getUniqueId().toString();
                } else {
                    senderuuid = fakeuuid;
                }
                if (args.length == 2) {
                    int ruleid = 0;
                    try {
                        ruleid = Integer.valueOf(args[1]);
                    } catch (NumberFormatException ex) {
                        ruleid = warnruleManager.getIdByName(args[1]);
                    }
                    if (ruleid == -1 || ruleid == 0) {
                        sender.sendMessage("Ungültige Banregel!");
                        return true;
                    }
                    warnManager.addWarn(UUID.fromString(uuid), UUID.fromString(senderuuid), warnruleManager.getWarnrule(ruleid), false);
                    return true;
                } else if (args.length >= 4) {
                    int time;
                    try {
                        time = Integer.valueOf(args[1]);
                    } catch (NumberFormatException exception) {
                        sender.sendMessage("/warn <player> <time> <penalty points> <reason>");
                        return true;
                    }
                    int penalytPoints;
                    try {
                        penalytPoints = Integer.valueOf(args[2]);
                    } catch (NumberFormatException exception) {
                        sender.sendMessage("/warn <player> <time> <penalty points> <reason>");
                        return true;
                    }
                    String warnmessage = "";
                    for (int i = 2; i < args.length; ++i) {
                        warnmessage += args[i];
                        warnmessage += " ";
                    }
                    warnManager.addWarn(UUID.fromString(uuid), UUID.fromString(senderuuid), warnmessage, time * 60, penalytPoints, false);
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
        return false;
    }
}
