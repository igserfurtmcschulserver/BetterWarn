package de.cag_igs.betterwarn.spigot.command;

import de.cag_igs.betterwarn.Utility;
import de.cag_igs.betterwarn.data.Banrule;
import de.cag_igs.betterwarn.manager.BanruleManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.List;

public class BanruleCommand implements CommandExecutor {

    private BanruleManager banruleManager;

    public BanruleCommand() {
        banruleManager = new BanruleManager();
    }

    private void paginateOutput(List<Banrule> banrules, int page, CommandSender executor) {
        int pages = banrules.size() / 5;
        executor.sendMessage("ID) Regelname: Grund [Bannzeit]");
        for (int i = page * 5; i < page * 5 + 5; ++i) {
            if (i < banrules.size()) {
                executor.sendMessage("" + banrules.get(i).getId() + ") " + banrules.get(i).getName() + ": " +
                        banrules.get(i).getReason() + " [" + banrules.get(i).getTime() + "]");
            }
        }
        executor.sendMessage("Seite: " + (page + 1) + "/" + (pages + 1));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!label.equalsIgnoreCase("banrule") || args.length < 1) {
            return false;
        }

        if (args[0].equalsIgnoreCase("list")) {
            if (sender.hasPermission("betterwarn.banrule.list")) {
                List<Banrule> banrules = banruleManager.getBanrules();
                int page = 0;
                if (args.length == 2) {
                    try {
                        page = Integer.parseInt(args[1]) - 1;
                        if (page < 0) {
                            page = 0;
                        }
                    } catch (NumberFormatException exception) {
                        sender.sendMessage("/banrule list [page]");
                        return true;
                    }
                }
                if (banrules != null) {
                    paginateOutput(banrules, page, sender);
                } else {
                    sender.sendMessage("Keine Bannregel vorhanden!");
                }
            }
            return true;
        } else if (args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("add")) {
            if (sender.hasPermission("betterwarn.banrule.create")) {
                if (args.length <= 5) {
                    sender.sendMessage("/" + label + " create " + " <name> <time (in minutes)|0> <message>");
                    return true;
                }
                String name = args[1];
                int time;
                try {
                    time = Integer.valueOf(args[2]) * 60;
                } catch (NumberFormatException except) {
                    sender.sendMessage("/" + label + " create " + " <name> <time (in minutes)|0> <message>");
                    return true;
                }
                String message = "";
                for (int i = 3; i < args.length; ++i) {
                    message += args[i];
                    message += " ";
                }
                banruleManager.addBanrule(name, message, time);
            }
            return true;

        } else if (args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("delete")) {
            if (sender.hasPermission("betterwarn.banrule.remove")) {
                if (args.length != 2) {
                    sender.sendMessage("Du musst eine ID angeben!");
                    return false;
                }
                int id;
                try {
                    id = Integer.valueOf(args[1]);
                } catch (NumberFormatException exception) {
                    sender.sendMessage("/banrule remove <id>");
                    return true;
                }
                banruleManager.deleteBanrule(id);
            }
            return true;
        } else {
            return false;
        }
    }
}
