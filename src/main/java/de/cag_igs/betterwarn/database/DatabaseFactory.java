package de.cag_igs.betterwarn.database;


import net.md_5.bungee.api.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class DatabaseFactory {
    private static IDatabase databaseObject = null;

    public static IDatabase createDatabaseObject(JavaPlugin plugin) {
        if (plugin.getConfig().getString("datastore.type").equalsIgnoreCase("mysql") && databaseObject == null) {
            databaseObject = new MySql(plugin.getName());
        }
        return databaseObject;
    }

    /*public static IDatabase createDatabaseObject(Plugin plugin) {
        //TODO: replace true with config value
        if (true) {
            databaseObject = new MySql(plugin.getDescription().getName());
        }
        return databaseObject;
    }*/

    public static IDatabase getDatabaseObject() {
        return databaseObject;
    }
}
