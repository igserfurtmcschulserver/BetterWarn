package de.cag_igs.betterwarn.database;

import java.sql.ResultSet;

/**
 * Created by Markus on 16.03.2017.
 */
public interface IDatabase {
    boolean connect();

    boolean isConnected();

    void close();

    boolean doUpdate(String sql);

    boolean doSafeUpdate(String sql, String... args);

    ResultSet doQuery(String sql);

    ResultSet doSafeQuery(String sql, String... args);
}
