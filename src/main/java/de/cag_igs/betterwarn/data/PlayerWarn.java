package de.cag_igs.betterwarn.data;

public class PlayerWarn {
    private int id;
    private String playerUuid;
    private String executerUuid;
    private String reason;
    private int bantime;
    private int time;
    private boolean active;
    private int penaltyPoints;

    public PlayerWarn(int id, String playerUuid, String executerUuid, String reason, int bantime, int time, boolean active,
                      int penaltyPoints) {
        this.id = id;
        this.playerUuid = playerUuid;
        this.executerUuid = executerUuid;
        this.reason = reason;
        this.bantime = bantime;
        this.time = time;
        this.active = active;
        this.penaltyPoints = penaltyPoints;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayerUuid() {
        return playerUuid;
    }

    public void setPlayerUuid(String playerUuid) {
        this.playerUuid = playerUuid;
    }

    public String getExecuterUuid() {
        return executerUuid;
    }

    public void setExecuterUuid(String executerUuid) {
        this.executerUuid = executerUuid;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getBantime() {
        return bantime;
    }

    public void setBantime(int bantime) {
        this.bantime = bantime;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getPenaltyPoints() {
        return penaltyPoints;
    }

    public void setPenaltyPoints(int penaltyPoints) {
        this.penaltyPoints = penaltyPoints;
    }
}
