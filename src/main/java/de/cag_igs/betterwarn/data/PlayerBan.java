package de.cag_igs.betterwarn.data;

public class PlayerBan {
    private int id;
    private String playerUuid;
    private String executerUuid;
    private String reason;
    private int bantime;
    private int time;
    private boolean active;

    public PlayerBan(int id, String playerUuid, String executerUuid, String reason, int bantime, int time, boolean active) {
        this.id = id;
        this.playerUuid = playerUuid;
        this.executerUuid = executerUuid;
        this.reason = reason;
        this.bantime = bantime;
        this.time = time;
        this.active = active;
    }

    /**
     * Returns the numeric ban id from the database.
     *
     * @return the ban id
     */
    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the uuid of the banned player.
     *
     * @return banned player uuid
     */
    public String getPlayerUuid() {
        return playerUuid;
    }

    public void setPlayerUuid(String playerUuid) {
        this.playerUuid = playerUuid;
    }

    /**
     * Returns the uuid of the executing player (the player who invoked the ban)
     *
     * @return executing player uuid
     */
    public String getExecuterUuid() {
        return executerUuid;
    }

    public void setExecuterUuid(String executerUuid) {
        this.executerUuid = executerUuid;
    }

    /**
     * Returns the reason of the ban.
     *
     * @return reason of the ban
     */
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * Returns the time when the player was banned as unix time stamp.
     *
     * @return time when the ban occurred.
     */
    public int getBantime() {
        return bantime;
    }

    public void setBantime(int bantime) {
        this.bantime = bantime;
    }

    /**
     * Returns how long the player should be banned as unix time.
     *
     * @return time how long the player should be banned
     */
    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    /**
     * Returns if the ban is still active or not.
     *
     * @return active state of the ban
     */
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
