package de.cag_igs.betterwarn.data;

public class Warnrule {
    private int id;
    private String name;
    private String reason;
    private int time;
    private int penaltyPoints;

    public Warnrule(int id, String name, String reason, int time, int penaltyPoints) {
        this.id = id;
        this.name = name;
        this.reason = reason;
        this.time = time;
        this.penaltyPoints = penaltyPoints;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getPenaltyPoints() {
        return penaltyPoints;
    }

    public void setPenaltyPoints(int penaltyPoints) {
        this.penaltyPoints = penaltyPoints;
    }
}
