/*package de.cag_igs.betterwarn.manager;

import de.cag_igs.betterwarn.RuntimeInformation;
import de.cag_igs.betterwarn.data.PlayerWarn;
import de.cag_igs.betterwarn.database.DatabaseFactory;
import de.cag_igs.betterwarn.database.IDatabase;
import de.cag_igs.betterwarn.database.MySql;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.sql.ResultSet;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DatabaseFactory.class})
public class WarnManagerTest {

    private IDatabase database;
    private MySql mockMysql;
    private DatabaseFactory mockDatabaseFactory;
    private RuntimeInformation runtimeInformation;

    private WarnManager warnManager;
    private int id = 42;
    private int nonid = 43;

    private ResultSet mockResultSet;

    private PlayerWarn singleWarn;
    @Before
    public void setUp() throws Exception {
        mockMysql = mock(MySql.class);
        database = mockMysql;
        singleWarn = new PlayerWarn(42, UUID.randomUUID().toString(),UUID.randomUUID().toString(), "This is a valid" +
                " reason", 12345, 1000, true, 5);

        runtimeInformation = mock(RuntimeInformation.class);
        PowerMockito.mockStatic(DatabaseFactory.class);
        PowerMockito.when(DatabaseFactory.getDatabaseObject()).thenReturn(database);

        when(mockMysql.doSafeUpdate(eq("UPDATE 'betterwarn_warns' SET 'active'=? WHERE " +
                "'id'=?"), eq(Boolean.toString(true)), eq(Integer.toString(id)))).thenReturn(true);
        when(mockMysql.doSafeUpdate(eq("UPDATE 'betterwarn_warns' SET 'active'=? WHERE " +
                "'id'=?"), eq(Boolean.toString(false)), eq(Integer.toString(id)))).thenReturn(true);
        when(mockMysql.doSafeUpdate(eq("UPDATE 'betterwarn_warns' SET 'active'=? WHERE " +
                "'id'=?"), eq(Boolean.toString(true)), eq(Integer.toString(nonid)))).thenReturn(false);
        when(mockMysql.doSafeUpdate(eq("UPDATE 'betterwarn_warns' SET 'active'=? WHERE " +
                "'id'=?"), eq(Boolean.toString(false)), eq(Integer.toString(nonid)))).thenReturn(false);

        mockResultSet = mock(ResultSet.class);
        when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        when(mockResultSet.getInt("id")).thenReturn(singleWarn.getId());
        when(mockResultSet.getString("playerUuid")).thenReturn(singleWarn.getPlayerUuid());
        when(mockResultSet.getString("executerUuid")).thenReturn(singleWarn.getExecuterUuid());
        when(mockResultSet.getString("reason")).thenReturn(singleWarn.getReason());
        when(mockResultSet.getInt("bantime")).thenReturn(singleWarn.getBantime());
        when(mockResultSet.getInt("time")).thenReturn(singleWarn.getTime());
        when(mockResultSet.getBoolean("active")).thenReturn(singleWarn.isActive());
        when(mockResultSet.getInt("penaltyPoints")).thenReturn(singleWarn.getPenaltyPoints());
        when(mockMysql.doSafeQuery(eq("SELECT * FROM 'betterwarn_warns' WHERE 'id'=?"), eq(Integer.toString(id))))
                .thenReturn(mockResultSet);
        when(mockMysql.doSafeQuery(eq("SELECT * FROM 'betterwarn_warns' WHERE 'id'=?"), eq(Integer.toString(nonid))))
                .thenReturn(null);


        warnManager = new WarnManager(runtimeInformation);
    }

    @Test
    public void testGetWarns() throws Exception {

    }

    @Test
    public void testValidateWarn() throws Exception {

    }

    @Test
    public void testValidateWarns() throws Exception {

    }

    @Test
    public void testValidateWarns1() throws Exception {

    }

    @Test
    public void testGetWarn() throws Exception {
        assertNull(warnManager.getWarn(nonid));
        assertEquals(singleWarn, warnManager.getWarn(id));
    }

    @Test
    public void testAddWarn() throws Exception {
        PlayerWarn temp = new PlayerWarn(0, UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                "This is a valid reason", 12345, 1000, true, 5);
        warnManager.addWarn(temp);
        verify(runtimeInformation, times(1)).dispatchPlayerWarnEvent(temp);
        verify(mockMysql, times(1)).doSafeUpdate(anyString(), eq(temp.getPlayerUuid()),
                eq(temp.getExecuterUuid()), eq(temp.getReason()), eq(Integer.toString(temp.getBantime())),
                eq(Integer.toString(temp.getTime())), eq(Boolean.toString(true)), eq(Integer.toString(temp.getPenaltyPoints())));
    }

    @Test
    public void testToggleWarn() throws Exception {
        assertTrue(warnManager.toggleWarn(id, true));
        assertTrue(warnManager.toggleWarn(id, false));
        assertFalse(warnManager.toggleWarn(nonid, true));
        assertFalse(warnManager.toggleWarn(nonid, false));
        verify(mockMysql, times(2)).doSafeUpdate(anyString(), Boolean.toString(anyBoolean()), eq(Integer.toString
                (id)));
        verify(mockMysql, times(2)).doSafeUpdate(anyString(), Boolean.toString(anyBoolean()), eq(Integer.toString
                (nonid)));
    }
}
*/